import { NgModule } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { AuthenticationRouting } from './authentication.routing';

@NgModule({
    declarations: [
        LoginComponent
    ],
    imports: [
        AuthenticationRouting
    ],
    exports: []
})
export class AuthenticationModule { }