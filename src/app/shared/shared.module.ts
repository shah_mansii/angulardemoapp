import { NgModule } from '@angular/core';
import { AuthenticationModule } from '../pages/authentication/authentication.module';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        AuthenticationModule],
    exports: [AuthenticationModule],
    providers: []


})
export class SharedModule { }